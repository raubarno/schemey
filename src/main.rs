// Copyright (c) 2024 Arnoldas Rauba
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the “Software”), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

mod parser;

use std::io::{stderr, BufRead, Write};

use is_terminal::IsTerminal;
use reedline::{DefaultPrompt, Reedline, Signal};

fn process_line(input: String) {
    if let Some(ast) = parser::parse(input.as_bytes()) {
        let mut ss = vec![(&ast, 0)];
        loop {
            let (top, depth) = if let Some((top, depth)) = ss.pop() {
                (top, depth)
            } else {
                break;
            };
            let head = format!(
                "{}{:?}",
                String::from_iter(vec![' '; depth * 2]),
                (if let parser::Symbol::NonTerm(nterm) = top.sym {
                    Some(nterm)
                } else {
                    None
                })
                .unwrap()
            );
            println!(
                "{:40}   {}",
                head,
                if top.terms.is_empty() {
                    "".to_owned()
                } else {
                    format!(
                        "{:?}",
                        String::from_iter(top.terms.iter().map(|&x| x as char))
                    )
                }
            );
            for child in top.children.iter() {
                ss.push((child, depth + 1));
            }
        }
    }
}

fn tty_main() {
    eprintln!("Schemey version {}", env!("CARGO_PKG_VERSION"));
    eprintln!("Press Ctrl+C to Exit");
    eprintln!();
    let mut line_editor = Reedline::create();
    let prompt = DefaultPrompt::default();
    loop {
        eprint!("schemey> ");
        stderr().flush().unwrap();
        let sig = line_editor.read_line(&prompt);
        match sig {
            Ok(Signal::Success(in_line)) => process_line(in_line),
            Ok(Signal::CtrlD) => {
                eprintln!("\nExit");
                break;
            }
            Ok(Signal::CtrlC) => {
                eprintln!("\nInterrupt");
                break;
            }
            x => {
                eprintln!("Event: {:?}", x);
            }
        }
    }
}

fn notty_main() {
    let stdin = std::io::stdin();
    let mut in_lines = stdin.lock().lines();
    loop {
        match in_lines.next() {
            Some(Ok(in_line)) => {
                process_line(in_line);
                print!("\n")
            }
            Some(Err(e)) => panic!("{}", e),
            None => break,
        }
    }
}

fn main() {
    if std::io::stdin().is_terminal() {
        eprintln!("Using interactive mode");
        tty_main()
    } else {
        eprintln!("Using non-interactive mode");
        notty_main()
    }
}
