//! An LL1 generic LISP-like parser
//
// Copyright (c) 2024 Arnoldas Rauba
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the “Software”), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

use smallvec::SmallVec;

#[repr(u8)]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum TermType {
    Character,
    LParen,
    RParen,
    Space,
    Unknown,
}

pub fn term_index(x: u8) -> TermType {
    match x as char {
        'A'..='Z' | 'a'..='z' | '0'..='9' => TermType::Character,
        '(' => TermType::LParen,
        ')' => TermType::RParen,
        ' ' | '\t' | '\r' | '\n' | '\0' => TermType::Space,
        _ => TermType::Unknown,
    }
}

#[repr(u8)]
#[derive(Clone, Copy, Debug)]
pub enum NonTermType {
    WordBody,
    Word,
    Whitesp,
    List,
    Tuple,
    Value,
    Document,
}

#[repr(u8)]
#[derive(Clone, Copy, Debug)]
pub enum Symbol {
    Term(TermType),
    NonTerm(NonTermType),
}

#[derive(Debug)]
pub struct AstNode {
    /// Type of nonterminal represented by this node.
    pub sym: Symbol,
    /// Terms consumed by the node.
    pub terms: SmallVec<[u8; 8]>,
    pub children: Vec<AstNode>,
}

impl AstNode {
    pub fn new(sym: Symbol) -> Self {
        Self {
            sym,
            terms: SmallVec::new(),
            children: Vec::new(),
        }
    }
}

pub fn create_flat_ast(text: &[u8]) -> Option<Vec<(AstNode, usize)>> {
    let mut ss = Vec::<(AstNode, usize)>::new();
    let mut ps = Vec::<(AstNode, usize)>::new();
    let add_nonterm = |ss: &mut Vec<(AstNode, usize)>, itype: NonTermType, rid: usize| {
        ss.push((AstNode::new(Symbol::NonTerm(itype)), rid))
    };
    let add_term = |ss: &mut Vec<(AstNode, usize)>, itype: TermType, rid: usize| {
        ss.push((AstNode::new(Symbol::Term(itype)), rid))
    };
    add_nonterm(&mut ss, NonTermType::Document, 0);
    for (idx, &chr) in text.iter().enumerate() {
        loop {
            let (mut top, rid) = if let Some((top, rid)) = ss.pop() {
                (top, rid)
            } else {
                break;
            };
            let r = ps.len();
            match top.sym {
                Symbol::Term(term) => {
                    if term == term_index(chr) {
                        top.terms.push(chr);
                        ps.push((top, rid));
                        break;
                    } else {
                        eprintln!(
                            "{}\n{}^--ERROR: Unexpected token\n",
                            String::from_iter(text.iter().map(|&x| x as char)),
                            String::from_iter(vec![' '; idx])
                        );
                        return None;
                    }
                }
                Symbol::NonTerm(nterm) => {
                    match (nterm, term_index(chr)) {
                        (NonTermType::WordBody, TermType::Character) => {
                            // => 0   WordBody ->  character ++ WordBody
                            add_nonterm(&mut ss, NonTermType::WordBody, r);
                            add_term(&mut ss, TermType::Character, r);
                        }
                        (NonTermType::WordBody, _) => {
                            // => 1   WordBody ->  epsilon
                        }
                        (NonTermType::Word, TermType::Character) => {
                            // => 2   Word     ->  character ++ WordBody
                            add_nonterm(&mut ss, NonTermType::WordBody, r);
                            add_term(&mut ss, TermType::Character, r);
                        }
                        (NonTermType::Word, _) => {
                            // => Unreachable, if not returned by a child (back?)
                            eprintln!(
                                "{}\n{}^--WARNING: Unreachable destination reached\n",
                                String::from_iter(text.iter().map(|&x| x as char)),
                                String::from_iter(vec![' '; idx])
                            );
                        }
                        (NonTermType::Whitesp, TermType::Space) => {
                            // => 3   Whitesp  ->  space ++ Whitesp
                            add_nonterm(&mut ss, NonTermType::Whitesp, r);
                            add_term(&mut ss, TermType::Space, r);
                        }
                        (NonTermType::Whitesp, _) => {
                            // => 4   Whitesp  ->  epsilon
                        }
                        (NonTermType::List, TermType::Character)
                        | (NonTermType::List, TermType::LParen) => {
                            // => 5   List     ->  Value ++ Whitesp ++ List
                            add_nonterm(&mut ss, NonTermType::List, r);
                            add_nonterm(&mut ss, NonTermType::Whitesp, r);
                            add_nonterm(&mut ss, NonTermType::Value, r);
                        }
                        (NonTermType::List, _) => {
                            // => 6   List     ->  epsilon
                        }
                        (NonTermType::Tuple, TermType::LParen) => {
                            // => 8   Tuple    ->  lparen ++ Whitesp ++ List ++ Whitesp ++ rparen
                            add_term(&mut ss, TermType::RParen, r);
                            add_nonterm(&mut ss, NonTermType::Whitesp, r);
                            add_nonterm(&mut ss, NonTermType::List, r);
                            add_nonterm(&mut ss, NonTermType::Whitesp, r);
                            add_term(&mut ss, TermType::LParen, r);
                        }
                        (NonTermType::Tuple, _) => {
                            // => Unreachable, if not returned by a child (back?)
                            eprintln!(
                                "{}\n{}^--WARNING: Unreachable destination reached\n",
                                String::from_iter(text.iter().map(|&x| x as char)),
                                String::from_iter(vec![' '; idx])
                            );
                        }
                        (NonTermType::Value, TermType::Character) => {
                            // => 9   Value    ->  Word
                            add_nonterm(&mut ss, NonTermType::Word, r);
                        }
                        (NonTermType::Value, TermType::LParen) => {
                            // => A   Value    ->  Tuple
                            add_nonterm(&mut ss, NonTermType::Tuple, r);
                        }
                        (NonTermType::Value, _) => {
                            // => Unreachable, if not returned by a child (back?)
                            eprintln!(
                                "{}\n{}^--WARNING: Unreachable destination reached\n",
                                String::from_iter(text.iter().map(|&x| x as char)),
                                String::from_iter(vec![' '; idx])
                            );
                        }
                        (NonTermType::Document, _) => {
                            // => B   Document ->  Whitesp ++ Value ++ Whitesp ++ EOF
                            add_nonterm(&mut ss, NonTermType::Whitesp, r);
                            add_nonterm(&mut ss, NonTermType::Value, r);
                            add_nonterm(&mut ss, NonTermType::Whitesp, r);
                        }
                    };
                }
            }
            ps.push((top, rid));
        }
    }
    Some(ps)
}

pub fn parse(text: &[u8]) -> Option<AstNode> {
    let mut ps = if let Some(ps) = create_flat_ast(text) {
        ps
    } else {
        return None;
    };
    loop {
        let (top, rid) = if let Some((top, rid)) = ps.pop() {
            (top, rid)
        } else {
            unreachable!()
        };
        if ps.is_empty() {
            return Some(top);
        }
        match top.sym {
            Symbol::Term(_) => ps[rid].0.terms.extend(top.terms),
            Symbol::NonTerm(_) => ps[rid].0.children.push(top),
        }
    }
}
